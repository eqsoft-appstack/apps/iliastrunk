#!/usr/bin/env bash

# init before stack is starting

. ./env

echo "init ilias config.json"

$CONTAINER_ENV run --rm --network=${NET} -w /tmp/config -v ${APP_ID}-data:/tmp/data -v $(pwd)/files/config:/tmp/config -u root --env-file=.env ${CONFIG_IMAGE} ./config.sh
