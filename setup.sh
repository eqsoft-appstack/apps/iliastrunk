#!/usr/bin/env bash

# setup after stack is started

. ./env

# check if ilias.ini.php exists from within php container

ILIAS_INI_EXISTS=$($CONTAINER_ENV exec -t -w /var/www/html ${APP_ID}-php [ -f ilias.ini.php ] && echo 1 )

if [ "$ILIAS_INI_EXISTS" == "1" ] ; then
    echo "ilias.ini.php already exists, skipping initial ilias setup..."
else
    echo "initial ilias setup..."
    $CONTAINER_ENV exec -t -w /var/www/html ${APP_ID}-php composer install -n
    $CONTAINER_ENV exec -t -w /var/www/html ${APP_ID}-php php setup/setup.php install -y /var/www/data/config.json
    #$CONTAINER_ENV exec -t -w /var/www/html ${APP_ID}-php composer du
fi
